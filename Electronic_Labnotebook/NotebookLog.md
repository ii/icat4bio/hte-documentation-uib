**Author**: Illimar Rekand \
**email**: illimar.rekand@uib.no \
**Description**: Electronic Lab Notebook for Illimar Rekand, working on establishing a closed loop system with the Unchained Labs at HTE@UiB. Entries are written chronologically from top to bottom.

**URL for this notebook**: https://git.app.uib.no/ii/icat4bio/hte-documentation-uib/-/blob/676904146ac8a3733bc6a0734da065cf6eab0441/Electronic_Labnotebook/NotebookLog.md

<!-- The TOC below will autoupdate if saved in VSCode with the Markdown-all-in-one extension installed -->

<details> 
  <summary> Table of Contents (click to expand) </summary>

- [Entries 2025](#entries-2025)
  - [2025-02-18](#2025-02-18)
    - [Entry 1: Installing new version of LSAPI (v1.3)](#entry-1-installing-new-version-of-lsapi-v13)
  - [2025-02-14](#2025-02-14)
    - [Entry 1: Parameter Map Editing bug fix](#entry-1-parameter-map-editing-bug-fix)
  - [2025-02-11](#2025-02-11)
  - [2025-02-09](#2025-02-09)
  - [2025-02-04](#2025-02-04)
    - [Entry 1:  Python Error while running experiment in AS10](#entry-1--python-error-while-running-experiment-in-as10)
  - [2025-01-22](#2025-01-22)
    - [Entry 1: Meeting with David (EditExps and more):](#entry-1-meeting-with-david-editexps-and-more)
  - [2025-01-15:](#2025-01-15)
    - [Entry 1: EditParameterMap fix and HPLC injection parameters](#entry-1-editparametermap-fix-and-hplc-injection-parameters)
  - [2025-01-14:](#2025-01-14)
    - [Entry 1: HPLC Dilution, EditParameterMap():](#entry-1-hplc-dilution-editparametermap)
  - [2025-01-07:](#2025-01-07)
    - [Entry 1: HPLC Dilution assay \& EditExps RunExp feature](#entry-1-hplc-dilution-assay--editexps-runexp-feature)
  - [2025-01-02](#2025-01-02)
    - [Entry 1: HPLC dilution assay](#entry-1-hplc-dilution-assay)
- [Entries 2024](#entries-2024)
  - [2024-12-31](#2024-12-31)
    - [Entry 1: Dictionary](#entry-1-dictionary)
  - [2024-12-12](#2024-12-12)
    - [Entry 1: Infrastructure/architecture](#entry-1-infrastructurearchitecture)
  - [2024-12-11](#2024-12-11)
    - [Entry 1: EditArrayMap fix](#entry-1-editarraymap-fix)
    - [Entry 2: Inspecting API function parameters](#entry-2-inspecting-api-function-parameters)
    - [Entry 3: Meeting with Unchained Labs (Docs, examples)](#entry-3-meeting-with-unchained-labs-docs-examples)
  - [2024-12-10](#2024-12-10)
    - [Entry 1: EditArrayMap issue](#entry-1-editarraymap-issue)
  - [2024-12-09:](#2024-12-09)
    - [Entry 1: Integrating PyAPI wrapper into HPLC dilution](#entry-1-integrating-pyapi-wrapper-into-hplc-dilution)
  - [2024-12-05:](#2024-12-05)
    - [Entry 1: PyAPI physical test](#entry-1-pyapi-physical-test)
  - [2024-12-04:](#2024-12-04)
    - [Entry 1: Running HPLC liquid handling assay](#entry-1-running-hplc-liquid-handling-assay)
    - [Entry 2: Weekly meeting with Unchained Labs (manually entering prompts/chem manager, Create Design w no DC, Substrate Error)](#entry-2-weekly-meeting-with-unchained-labs-manually-entering-promptschem-manager-create-design-w-no-dc-substrate-error)
  - [2024-11-29:](#2024-11-29)
    - [Entry 1: HPLC liquid handling assay](#entry-1-hplc-liquid-handling-assay)
  - [2024-11-28:](#2024-11-28)
    - [Entry 1: Updating settings for VM](#entry-1-updating-settings-for-vm)
    - [Entry 2: Setting manual prompts in the Python script](#entry-2-setting-manual-prompts-in-the-python-script)
  - [2024-11-27](#2024-11-27)
    - [Entry 1: Virtual Machine](#entry-1-virtual-machine)
    - [Entry 2: Weekly meeting with Unchained Labs](#entry-2-weekly-meeting-with-unchained-labs)
  - [2024-11-21](#2024-11-21)
    - [Entry 1: Physical test using exe scripts II](#entry-1-physical-test-using-exe-scripts-ii)
  - [2024-11-20](#2024-11-20)
    - [Entry 1: Weekly meeting with Unchained Labs](#entry-1-weekly-meeting-with-unchained-labs)
  - [2024-11-19](#2024-11-19)
    - [Entry 1: Physical test using exe scripts I](#entry-1-physical-test-using-exe-scripts-i)
  - [2024-11-18:](#2024-11-18)
    - [Entry 1:](#entry-1)
    - [Entry 2](#entry-2)
  - [2024-11-07:](#2024-11-07)
    - [Entry 1:](#entry-1-1)
    - [Entry 2:](#entry-2-1)
- [Current situation:](#current-situation)

</details>

## Entries 2025

### 2025-02-18 

#### Entry 1: Installing new version of LSAPI (v1.3)

Was unable to install the new version of LSAPI with the installer sent from UL. Deleted the files inside `C:/LSAPI/SDK/`, then reinstalled LSAPI using SetupLSAPI.msi. The resulting .dll file was then regenerated.

### 2025-02-14

#### Entry 1: Parameter Map Editing bug fix

Had a meeting with Unchained Labs regarding last issue: turned out this was due to a bug from the LSAPI; UL promised to share latest version (v 1.3) with us ASAP, which should fix this issue
### 2025-02-11

Issues with editing HPLC parameter map: in a MWE in a .ipynb, I try to edit a discrete text parameter map using EditParameterMap(), but after a succesful edit, I am unable to access the map using the GetMap()-function. Sent email to Bjorn and rest of the team.

### 2025-02-09

Entry 1: How to generate a LS10 experiment with dispensing maps prior to injection:

- Step 1: Create experiment in LS10 with *just* the dispense maps, and leave the HPLC injection maps out of the design
- Step 2: Then, finalize the design in Design Creator. After the DC wizard is done, this experiment has been added to the LEA database.
- Step 3: Then, add the HPLC dispense maps.
- Step 4: Finally, save the design to the LEA database with a new design ID. This new design should now be available 

### 2025-02-04

#### Entry 1:  Python Error while running experiment in AS10

Tested a run where experiment was started from the Unchained_labs package within the EditExps framework. Encountered the following error

 ![AS10 Python Error](https://git.app.uib.no/ii/icat4bio/hte-documentation-uib/-/raw/main/Electronic_Labnotebook/2025-02-04-AS-PythonError-Heated6TipBlock.png?ref_type=heads)

Seems to be fixed by simply closing and killing AS10 a few times...

### 2025-01-22 

#### Entry 1: Meeting with David (EditExps and more):

* from pathlib import path, os.path, suffix(file), <-- (xml)
* See if all relevant information can be retrieved through designID, try and hide LS10 specifics as much as possible from Layer1, use EditExps to retrieve this inform
* minimizing the architecture dependecies/redundancies. for example: if sys.arch =="64": import foo_64 as foo, else: import foo_32 as foo

### 2025-01-15:

#### Entry 1: EditParameterMap fix and HPLC injection parameters

Received an email from Unchained Labs to fix the previously mentioned issue with this code snippet:

```
def ConvertToListofObjectValues(value_list: list) -> System.Collections.Generic.List[Object]:

     count = len(value_list)
     dotnet_list = List[Object]()
     #Add each item from the Python list to the .NET List
     for item in value_list:
                    dotnet_list.Add(item)                               
     return dotnet_list 
```

Added this now to the codebase. However, encountered following issues:

* the values of each cell need to be updated with a string for 3 individual parameter maps, which :
  1) directs to the HPLC method, 
  2) specifies the number of replicates and
  3) specifies the injection volume.
  
  Each of these maps are applied to cells relevant for the HPLC injection. My current idea is to write one function which addresses all of these maps.
* 

### 2025-01-14:

#### Entry 1: HPLC Dilution, EditParameterMap():

* Tried to update existing map, but EditParameterMap gives an error: 

```
PythonException: System.Collections.Generic.List`1[System.Double] value cannot be converted to System.Collections.Generic.List`1[System.Object]
```
Seems like this stems from taht the function utils.AmountForDiscrete() does not output the right format for this function, while it does work for other functions, such as EditSourceMap(). Sent an email to Bjorn, hoping to clarify this. 


### 2025-01-07:

#### Entry 1: HPLC Dilution assay & EditExps RunExp feature

* EditExps can now both update liquid dispensing maps **and** start running an experiment:
  ```mermaid
  flowchart TD;
    Layer1-->|FunctionName </br> which function from ls10 to execute |EE{EditExps}
    EE-->|FunctionName: e.g. 'EditArrayMapDiscrete'|EM[Edit dispense maps in experiment in LS10]
    EE-->|FunctionName: e.g. 'EditParameterMap'|EP[Edit parameters for experiments in LS10]
    EE-->|FunctionName: e.g. 'RunExperiment'|RE[Run Experiment in AS10]
  ```
  This means now that only one script is necessary to be used for editing liquid maps **and** running experiments, with added flexibility.
* EditExps also includes now a feature which autogenerates a promptfile from a chemical manager file.

### 2025-01-02

#### Entry 1: HPLC dilution assay

Starting integrating the previous scripts into this assay. Wrapped the os.system() command in a function called "runEditExps" (I really think EditExps should be renamed, but to what?)

Further adaptations made to EditExps so that it can also run experiment - idea is to create one single script which can perform all sorts of operations, including editing and running experiment, essentially replacing the .exe-files and having one "superscript" which can be reused for any given experiment.

## Entries 2024

### 2024-12-31

#### Entry 1: Dictionary

Input is now added via a dictionary. This makes it possible to create default values which are overwritten if optional values are given. 

### 2024-12-12

#### Entry 1: Infrastructure/architecture

Worked on some infrastructure; creating scripts which can pass parameters on to a conda-script. 

```mermaid
flowchart TD;
    subgraph 64-bit Conda environment
      subgraph Layer 1  
          Comment1(Calculations, e.g. volume, are performed and passed down in input_dict )
          Layer1.py 
      end
    end
    subgraph 32-bit Conda environment
      subgraph Layer 2
        Comment2(Single action scripts are performed, e.g. edit dispense map, start experiment.)
        EditExps
      end
        subgraph Layer 3
        Comment3(Individual functions for editing maps, parameters, etc. are stored. Default values are defined.)
        ls10
      end
      Layer1.py-->|Required+Optional parameters|EditExps;
      EditExps-->ls10;
      subgraph Layer 4
        LSAPI
      end
      ls10-->|Datatypes prepared for C# + </br> Default optional requirements set if not defined|LSAPI;
    end
```


1) Layer1: Top layer, where calculations can be performed. 64-bit Python. Calculations and parameters are calculated here, and passed down.
2) Layer2: EditExps New parameters are sent to this layer, which are in a 32-bit environment. 
3) Layer3: ls10 In this layer, inputs are prepared to be compatible with the LSAPI
4) Layer4: In this layer, the parameters for a certain experiment is updated

### 2024-12-11

#### Entry 1: EditArrayMap fix

was able to fix this issue by inputting **13** input variables, not **12** as the UL documentation says. The MapIndex parameter, which is missing, is probably a typo.

#### Entry 2: Inspecting API function parameters

Created two (hopefully) useful functions, which can return info about functions inside the API:

* GetNumParameters(): Returns the number of input parameters which a function needs to be run
* GetParameterNames(): Returns the names of the parameters

These can be found in unchained_labs.utils

#### Entry 3: Meeting with Unchained Labs (Docs, examples)

Had a meeting with Unchained Labs. Presented the issue from the last few days, pointed out that the documentation contains typos.

Bjorn was interested in use cases for the closed loop, where they could present some mimimum working examples.

Also discussed which files can be shared openly (e.g. DLLs)

### 2024-12-10

#### Entry 1: EditArrayMap issue

Been having some issues with the EditArrayMap() function, called inside EditArrayMapDiscrete(). Although I've seemingly written the correct input to this function, the function returns an error:

`No method matches given argument for LibraryStudioWrapper.EditArrayMap`

Input for the function does seem to match what is given in the docs. Looked at this with David, we could not figure it out. Sent an email to Bjorn, asking for a meeting.

### 2024-12-09:

#### Entry 1: Integrating PyAPI wrapper into HPLC dilution

* Looked into how two different versions of python (32 and 64 bit) can be run in the same script, as the PyAPI wrapper needs to be run in a 32-bit env. Solution seems to be that this statement:

```
os.system("conda run -n closed-loop python .\EditExp362.py)
```

 will be placed inside the main Py-script, essentially acting as a .exe file. The statement above will run the script called "EditExp362.py" inside the conda env "closed-loop", which then acts on the Python API wrapper.
* Starting writing a new function for editing an array, using the EditArrayMap. This function is called EditArrayMapDiscrete()
* Also found that Jupyter Notebooks can be useful for debugging the UL PyAPI wrapper (`HPLC\debugging.ipynb`)

### 2024-12-05:

#### Entry 1: PyAPI physical test

ran `run_closed_loop_w_PyAPi.py` physical test, worked as expected :).

### 2024-12-04:

#### Entry 1: Running HPLC liquid handling assay

Tried to run the experiment below just from AS. This gives a GetSubstrate error. Seems to be affecting all experiments created in LS which are created w/o Design Creator, but experiments create with Design Creator run smoothly. Either somethings is broken or I am doing something wrong (see entry 2)

#### Entry 2: Weekly meeting with Unchained Labs (manually entering prompts/chem manager, Create Design w no DC, Substrate Error)

Three things discussed:

1) **How to manually enter prompts/chemical manager files:** \
    Absolute paths must always be given in order for the Sila browser to recognize the xmls, and therefore, relative paths must always be avoided.

2) **Design creations in LS without the use of Design Creator (DC) which resulted in Substrate Error:** \
 When creating a design without DC, the position and the type of the library must be defined **twice** in AS to avoid the GetSubstrate Error. See figure below:

 ![Specify Substrate Info](https://git.app.uib.no/ii/icat4bio/hte-documentation-uib/-/raw/main/Electronic_Labnotebook/2024-12-06-NoDC.png?ref_type=heads)

3) **Substrate Error when running `run_closed_loop_w_PyAPi.py`** \
This issue was fixed due to that the wrong tag for backsolvent was was used. Correct: **Backsolvent**, Incorrect: Backingsolvent.
When tested, this script worked with hardware disabled!

Other things: TagAll is a useful shortcut to go straight to the tags editor. Tags are **not** case-sensitive.

Following would be useful:

* Write a vocabulary in the wiki with an overview of all the terms used so far in this project
* Create tutorials in the wiki outlining how to create designs in LS, preferably with scren capture

### 2024-11-29:

#### Entry 1: HPLC liquid handling assay

In Library Studio, the following assay handles liquid for the HPLC dilution assay: 

* Design ID: 362
* Library IDs: 100202 ("starting material plate"), 100203 ("Sample plate")

This library design combines two dyes into the same well range, before they are injected in another library design.

Design:

![HPLC assay mixing Design](https://git.app.uib.no/ii/icat4bio/hte-documentation-uib/-/raw/main/Electronic_Labnotebook/2024-11-29-Mixing.png?ref_type=heads)

### 2024-11-28:

#### Entry 1: Updating settings for VM

Exported the settings via the archive file and uploaded it to the VM. Seemed to work well.

#### Entry 2: Setting manual prompts in the Python script
Exp344 = exp which Python edits. Exp358 = working exp with backsolvent. 

Tried to fix the Python script editing experiment 344. Although Valve and Syringe parameters are set, GetSubstrate error persists (see [this entry](#2024-11-07)). Checked difference between chemical manager files between 344 and 358, which looked OK. Tried to manually set the pormpt and chemical manager files in the EditExps script, but getting the following error: 

```
Exception: Invalid prompt file
```

which comes from the function `as_client.ExperimentService.SetPrompts(<path>).ReturnValue`. Tried to input the prompt file which is also written for Exp344, but this does not work either. I am perplexed over how this works. A similar function exists for the script using the executable files, and this seems to be working fine.

Also copied the entire content of Exp358Prompt into prompstpart1 (renamed to `promptspart1mod2.xml`) and read this into the experiment. Started run, did not complain about incorrect prompt file.

### 2024-11-27

#### Entry 1: Virtual Machine

For the last three days, I've been working on setting up a working instance on a virtual machine provided by Unchained Labs using Hyper V. Some important things I have learnt:


* start Hyper-V manager as admin, otherwise the VM will not be available.
* if left idle, the VM will shut down, and when reopened, you have to log in as if the machine had been turned off
* the setup for the Junior in this VM is not 1:1 to the physical machine we have in Bergen. As such, the tags need to be different (H6Tips instead of H6Tips) and the deck positions need to be changed.
* As a general test, test.run1() from the APItizer is a good indicator to check that things are working as they should
* The conda environment is behaving strangely and I've done some hacks to make things work:
   * First of all, the example 32-bit python.exe provided by Unchained Labs which is necessary to use the Python LSAPI has been added to PATH, and as such, **all testing should be done with conda deactivated**. 
   * To find the correct scripts and utilities, a line of sys.path.append() is added before import statements which specifies to site-packages. In an ideal world, custom packages would be installed locally in a `pip install -e .` fashion, but this is the solution I have used which works now.


Looking at the chemical manager file, I'm still unsure how good of a model the VM is for debugging and development. The valve resources in the chemical manager file when using backsolvent is not identical to what is given in the chemical manager in the PC for the Junior machine.

#### Entry 2: Weekly meeting with Unchained Labs

Following points were discussed:

* Mirror Junior settings in VM with our setup: export the archive files, then import them via the "hamburger menu" in Automation Studio
* Desired examples using the API were presented and discussed.


### 2024-11-21

#### Entry 1: Physical test using exe scripts II

With the suggestions from yesterday's meeting, a physical test was done with a new library id (350). Test was succesful.

### 2024-11-20

#### Entry 1: Weekly meeting with Unchained Labs

Had a weekly wednesday meeting with Unchained Labs (Present: Bjorn Monteen, Vy, John Thoits, Bren Ehnebuske)

Some things which emerged from this meeting:

* Valve positions/needle positions need to be specified in the Chemical Manager file in order fix the Get Substrate Properties Error.
* The chemical manager file which was used for the .exe-related script did not have covered state included in it. 
* Chemical maps can be edited for an existing experiment in Library Studio and re-saved. However, if an experiment is saved as a copy, this map cannot be saved

furthermore;

* Unchained asked if we could provide some specific examples related to experiments which should be run with the Closed Loop-project. They can write examples using the LSAPI which we can adapt and use for our own purposes, as the examples provided now are quite far from what we want to do 

### 2024-11-19

#### Entry 1: Physical test using exe scripts I

Ran a physical test of yesterday's script with hardware enabled. The wells are dispensed correctly, but a second map dispenses 1000 uL to all wells A1 to A6. This map is probably an artefact from the method used at UBC, which I did update to 1000 uL in my testing.

Tried updating experiment 116 with removing the second map from the GUI (Library Studio), however, it does not allow me to save the experiment with the same Design ID, giving the following error:

![Library Studio Error](https://git.app.uib.no/ii/icat4bio/hte-documentation-uib/-/raw/main/Electronic_Labnotebook/2024-11-19-ErrorDesignWasReplicated.png?ref_type=heads)


Tried editing the aforementioned python script (`2024-11-18-run_closed_loop_automation_UIB_discrete.py`) to work with experiment 349 (a similar experiment, but with only one map), but this resulted in the following error:

![Experiment Error](https://git.app.uib.no/ii/icat4bio/hte-documentation-uib/-/raw/main/Electronic_Labnotebook/2024-11-19-Exp349WithRunClosedLoop.png?ref_type=heads)

Tried saving a copy of Experiment 116 to a new library design (350). Reran the script using this library ID, resulted in the folowing error:

![Experiment 350 Error](https://git.app.uib.no/ii/icat4bio/hte-documentation-uib/-/raw/8a4a15e19f86e809a0ea364a9955d34acd182c5b/Electronic_Labnotebook/2024-11-19-Exp350WithRunClosedLoop.png)



### 2024-11-18:

#### Entry 1:
Tried modifying run_closed_loop_automation_UIB.py by replacing the .exe-part with the equivalent Python methods from EditExps in a new script called run_closed_loop_automation_UIB_no_exe_files.py, *ie.*:

replacing:

```python
   run([r'C:\Users\Unchained Labs\Desktop\LSAPI_exp116.exe', DESIGN_ID, step_number, values, DEST_START_CELL, DEST_END_CELL])
```

with

```python
    EditExps.LoadAndModifyExp(design_id,
                            volumes,
                            well_range,
                            plate_name,
                            tag_list, 
                            selected_unit)

```

This resulted in:

![Substrate Error](https://git.app.uib.no/ii/icat4bio/hte-documentation-uib/-/raw/8a4a15e19f86e809a0ea364a9955d34acd182c5b/Electronic_Labnotebook/2024-11-07-GetSubsrtrateError.png)

*ie.*, the same error as before

#### Entry 2

Rewrote the .exe file used in run_closed_loop_automation_UIB and rebuilt in Visual Studio. Major changes:
* updated the map index to 1 and 2 instead of 3 and 4 (maps 3 and 4 do not exist in Experiment 116)
* updated the map editing function to set a discrete map, not a uniform map
* updated the function to read the values as a list, so that the discrete map is updated with these volumes

The new file is called `2024-11-18-run_closed_loop_automation_UIB_discrete.py`

With hardware disabled, this script works, adding liquid to well A1 in the first iteration, then liquid to well A2.

### 2024-11-07:

#### Entry 1:
After a meeting the day before with Unchained labs, I was recommended to try and create an experiment in Library Studio with the Design Creator, save it to database, then save it to a file, then save it to the database again. Reran `run_closed_loop_w_PyAPI.py` with the new design_id (349). However, this produced the same error as before:

![Substrate Error](https://git.app.uib.no/ii/icat4bio/hte-documentation-uib/-/raw/8a4a15e19f86e809a0ea364a9955d34acd182c5b/Electronic_Labnotebook/2024-11-07-GetSubsrtrateError.png)

Comparing the chemical manager file for Experiment 349 and Experiment 344 reveals that they are identical.

#### Entry 2:
Tried to run `run_closed_loop_w_PyAPI.py` with an exported chemical manager file, by setting `chem_path` to `"./exp347-chemmanager"`
Get error: "Invalid chemical manager file". Stems from function `_checkResult(json.loads(as_client.ExperimentService.SetChemicalManager(chemical_manager_path).ReturnValue))`

Tried to manually set the chemical manager file to the one which the scripts writes automatically ("Exp344ChemFile.xml"). This produced the same error as above.

2024-06-11:

## Current situation:

- We have a script (run_closed_loop.py) inherited from the Hein group at the University of British Columbia, which should be able call and edit an existing experiment (Experiment 116, or "Exp116"). This script is a Python script which contains executables, written in C#, using the Unchained Labs API (aka. LSAPI). The issue in Bergen is that the executables/LSAPI do not work with the setup we have at UiB. In contact with Unchained Labs, we have been recommended to use their new Python-based API, which works as a wrapper around the C#-based API. Initial tests with this API in Bergen has been succesful, while as of per today's date, the .exe-files do not properly work as intended.

- 




