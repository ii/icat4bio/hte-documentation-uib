# HTE documentation UiB 🤖🧪📄

This repo is for storing documentation and serves as a general knowledge base for using the [High Throughput Experimental (HTE) platform at UiB](https://www.uib.no/en/kj/101028/laboratory-automated-chemistry) related to the closed-loop project associated with [iCat4Bio](https://prosjektbanken.forskningsradet.no/en/project/FORISS/331967?Kilde=FORISS&distribution=Ar&chart=bar&calcType=funding&Sprak=no&sortBy=date&sortOrder=desc&resultCount=30&offset=0&Soknad=Forskerprosjekt)

## Electronic Notebook

This repo contains an [electronic notebook (ELN)](https://git.app.uib.no/ii/icat4bio/hte-documentation-uib/-/blob/031dc3432b48957114cb3d8600d1697308df4baa/Electronic_Labnotebook/NotebookLog.md) which acts as a log for debugging and testing.

## Issues

In this [repo's issues](https://git.app.uib.no/Illimar.Rekand/hte-documentation-uib/-/issues), you can find an overview of various hurdles we have encountered in setting up the closed-loop project, and how we have eventually solved them.

## HTE-wiki

This [repo's wiki](https://git.app.uib.no/Illimar.Rekand/hte-documentation-uib/-/wikis/home) acts as a knowledge base for HTE-related points of interest.

## License

The contents of this repository is licensed under a Creative Commons license

## Ontology

Where applicable, we refer to the [Software Ontology (SWO)](https://github.com/allysonlister/swo), which is indexed in the [Ontology Lookup Service (OLS)](https://www.ebi.ac.uk/ols4/ontologies/swo)