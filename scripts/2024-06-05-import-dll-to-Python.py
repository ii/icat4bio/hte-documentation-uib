# Description: basic Python script for importing dll-functions into Python
# Created 2024-06-05 by Illimar Rekand, University of Bergen
# illimar.rekand@uib.no
# CC0 license

import clr  # Import the pythonnet module

# Add the path to the DLL
clr.AddReference(r'C:\LSAPI\SDK\LSAPI.dll')
from LS_API import LSAPI
api = LSAPI()
api.EditArrayMap()