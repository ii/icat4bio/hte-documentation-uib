# Contents

Last updated: 2024-08-15  
**2024-06-05-import-dll-to-Python.py**:  basic Python script for importing dll-functions into Python  
**LSAPI_exp116.cs:** C-sharp script used to compile the .exe files used in run_closed_loop_automation.py  
**run_closed_loop_automation.py:** Python-script used to start an automated loop. Is likely depreciated by the Python API explored in test_UiB.py.  
**test_UiB.py:** Demo script for the Unchained Labs Python API. Slightly modified to match the parameters for the Junior robot at UiB