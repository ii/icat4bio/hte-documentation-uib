
import time
from subprocess import run
from unchained_labs.automation_studio_api import (set_experiment_design_id, set_prompts, start_experiment_run,
                                                  get_experiment_status, wait_for_experiment_complete,
                                                  wait_for_annotating_experiments_action)
from unchained_labs.pyautogui_utils import ok_button_screen_location, move_and_left_click
if __name__ == '__main__':
    # constants
    DESIGN_ID = '100'
    DEST_START_CELL = '1,1'
    DEST_END_CELL = '1,12'
    print('configure first experiment, update values for steps 3 and 4')
    values = '100,0,0,0,0,0,0,0,0,0,0,0'  # volumes
    step_number = '3'
    run([r'C:\Users\Unchained Labs\source\repos\LSAPI_exp100\LSAPI_exp100\bin\Release\LSAPI_exp100.exe', DESIGN_ID, step_number, values, DEST_START_CELL, DEST_END_CELL])
    step_number = '4'
    run([r'C:\Users\Unchained Labs\source\repos\LSAPI_exp100\LSAPI_exp100\bin\Release\LSAPI_exp100.exe', DESIGN_ID, step_number, values, DEST_START_CELL, DEST_END_CELL])
    print('run experiment 1')
    r = set_experiment_design_id(100)
    print(r)
    r = set_prompts(r'C:\Users\Unchained Labs\PycharmProjects\sila2_test\run_AS.prompts.WithDesignCreator.xml')
    print(r)
    r = start_experiment_run()
    print(r)
    time.sleep(2)
    print(f'experiment status: {get_experiment_status().content}')
    print('wait for annotating experiments action')
    wait_for_annotating_experiments_action()
    print('locate, move mouse, and click on "ok" button')
    loc = None
    while loc is None:
        time.sleep(2)
        loc = ok_button_screen_location()
    for i in range(3):
        time.sleep(2)
        move_and_left_click(loc)
    print('wait for get_experiment_status to be experiment completed')
    wait_for_experiment_complete()
    print('experiment 1 done')
    print('configure second experiment, update values for steps 3 and 4')
    values = '0,100,0,0,0,0,0,0,0,0,0,0'  # volumes
    step_number = '3'
    run([r'C:\Users\Unchained Labs\source\repos\LSAPI_exp100\LSAPI_exp100\bin\Release\LSAPI_exp100.exe', DESIGN_ID, step_number, values, DEST_START_CELL, DEST_END_CELL])
    step_number = '4'
    run([r'C:\Users\Unchained Labs\source\repos\LSAPI_exp100\LSAPI_exp100\bin\Release\LSAPI_exp100.exe', DESIGN_ID, step_number, values, DEST_START_CELL, DEST_END_CELL])
    print('run experiment 2')
    r = set_experiment_design_id(100)
    print(r)
    r = set_prompts(r'C:\Users\Unchained Labs\PycharmProjects\sila2_test\run_AS.prompts.WithDesignCreator.xml')
    print(r)
    r = start_experiment_run()
    print(r)
    time.sleep(2)
    print(f'experiment status: {get_experiment_status().content}')
    print('wait for annotating experiments action')
    wait_for_annotating_experiments_action()
    print('locate, move mouse, and click on "ok" button')
    loc = None
    while loc is None:
        time.sleep(2)
        loc = ok_button_screen_location()
    for i in range(3):
        time.sleep(2)
        move_and_left_click(loc)
    print('wait for get_experiment_status to be experiment completed')
    wait_for_experiment_complete()
    print('experiment 2 done')
    print('done')
